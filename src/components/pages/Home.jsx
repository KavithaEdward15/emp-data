import React, { useState, useEffect } from "react";
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import axios from "axios";
import { Link } from "react-router-dom";


const Home = () => {
    const [users, setUser] = useState([]);

    useEffect(() => {
        loadUsers();
    }, []);
    
   const onConfirmDelete = (user) => {
        confirmAlert({
        border : '4px',
          title: 'Confirm to submit',
          message: 'Are you sure to do this.',
          buttons: [
            {
              label: 'Yes',
              onClick: (id) => deleteUser(user.id)
            },
            {
              label: 'No',
            }
          ]
        });}

const [search, setSearch] = useState([]);
const [filteredEmployee, setFilteredEmployee] = useState([]);
useEffect(()=>{
    setFilteredEmployee(users.filter(User => 
    User.name.toLowerCase().includes(search))
    )
},[search, users]
)
    const loadUsers = async() => {
        const result = await axios.get("http://localhost:3002/users");
        setUser(result.data);
    };

    const deleteUser = async id => {
        await axios.delete(`http://localhost:3002/users/${id}`);
        loadUsers();
    };

    const elementStyle ={
        border:'solid',
        borderRadius:'10px',
        position:'relative',
        left:'2vh',
        height:'5vh',
        width:'32vh',
        marginTop:'2vh',
        marginBottom:'3vh'
      }
    return ( <div className = "container" >
        <div className = "py-4" >
        <h1 > Home Page </h1><input type="text" placeholder="Enter name to be searched" style={elementStyle} onChange= {e=> setSearch(e.target.value)}/>
        <table class = "table border shadow" >
        <thead class = "thead-dark" >
        <tr >
        <th scope = "col" > # </th> 
        <th scope = "col" > Name </th> 
        <th scope = "col" > Department Name </th> 
        <th scope = "col" > Salary </th> 
        <th scope = "col" > Phone Number </th> 
        <th scope = "col" > Date of Birth </th> 
        <th > Action </th> </tr > </thead> 
        <tbody> {filteredEmployee.length ?
        filteredEmployee.map((user, index) => <tr>
                <th key = "row" > { index + 1 } </th> 
                <td > { user.name } </td> <td> { user.department } </td> 
                <td> { user.salary } </td>
                <td> { user.phone } </td> <td> { user.dob } </td>
                 <td >
                <Link class = "btn btn-outline-primary mr-2"
                to = { `/users/edit/${user.id}` } >
                Edit </Link> 
                <Link class = "btn btn-danger" onClick = {
                    () => onConfirmDelete(user)}  >
                Delete </Link> </td > </tr>
            ) : <tr><td></td><td></td> <td>No record found </td></tr> 
        }
        
         </tbody> 
         </table > </div> </div >
    );
};

export default Home;