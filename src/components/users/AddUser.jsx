import React, { useState } from "react";
import axios from 'axios'
import { useHistory } from "react-router-dom";
import moment from "moment";

const AddUser = () => {
    let history = useHistory();
    const [user, setUser] = useState({
        name: "",
        department: "",
        salary: "",
        phone: "",
        dob: ""
    });
    let dateRestrict = moment().subtract(22, 'years').format('YYYY-MM-DD')
    const { name, department, salary, phone, dob } = user;
    const onInputChange = e => {
        setUser({...user, [e.target.name]: e.target.value });
    };

    const onSubmit = async e => {
        e.preventDefault();
        await axios.post("http://localhost:3002/users", user);
        history.push("/");
    };
    return ( <div className = "container" >
        <div className = "w-75 mx-auto shadow p-5" >
        <h2 className = "text-center mb-4" > Add Employee </h2> <form onSubmit = { e => onSubmit(e) } >
        <div className = "form-group" >
        <input type = "text" pattern="[A-Z a-z]{5,50}"
        className = "form-control form-control-lg"
        placeholder = "Enter Your Name"
        name = "name"
        value = { name }
        onChange = { e => onInputChange(e) } required/>
        </div > <div className = "form-group" >
        <input type = "text" pattern="[A-Za-z]{2,25}"
        className = "form-control form-control-lg"
        placeholder = "Enter Your Department"
        name = "department"
        value = { department }
        onChange = { e => onInputChange(e) } required/> </div > 
        <div className = "form-group" >
        <input type = "number"
        className = "form-control form-control-lg"
        placeholder = "Enter Your Salary"
        name = "salary"
        value = { salary }
        onChange = { e => onInputChange(e) } required
        /> </div > <div className = "form-group" >
        <input type = "number"
        className = "form-control form-control-lg"
        placeholder = "Enter Your Phone Number"
        name = "phone"
        value = { phone }
        onChange = { e => onInputChange(e) } required
        /> </div > <div className = "form-group date" >
        <input type = "date" max={dateRestrict}
        className = "form-control form-control-lg"
        placeholder = "Enter Your DOB"
        name = "dob"
        value = { dob } 
        onChange = { e => onInputChange(e) } required
        /> </div > <button className = "btn btn-primary btn-block" > Add Employee </button> </form > </div> </div >
    );
};

export default AddUser;