import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import moment from "moment";

const EditUser = () => {
    let history = useHistory();
    const { id } = useParams();
    const [user, setUser] = useState({
        name: "",
        department: "",
        salary: "",
        phone: "",
        dob: ""
    });
    let dateRestrict = moment().subtract(22, 'years').format('YYYY-MM-DD')
    const { name, department, salary, phone, dob } = user;
    const onInputChange = e => {
        setUser({...user, [e.target.name]: e.target.value });
    };

    useEffect(() => {
        loadUser();
    },[]);

    const onSubmit = async e => {
        e.preventDefault();
        await axios.put(`http://localhost:3002/users/${id}`, user);
        history.push("/");
    };

    const loadUser = async() => {
        const result = await axios.get(`http://localhost:3002/users/${id}`);
        setUser(result.data);
    };
    return ( <div className = "container" >
        <div className = "w-75 mx-auto shadow p-5" >
        <h2 className = "text-center mb-4" > Edit Employee </h2> 
        <form onSubmit = { e => onSubmit(e) } >
        <div className = "form-group" >
        <input type = "text"
        className = "form-control form-control-lg"
        placeholder = "Enter Your Name"
        name = "name"
        value = { name }
        onChange = { e => onInputChange(e) }
        /> </div > 
        <div className = "form-group" >
        <input type = "text"
        className = "form-control form-control-lg"
        placeholder = "Enter Your department"
        name = "department"
        value = { department }
        onChange = { e => onInputChange(e) }
        /> </div > 
        <div className = "form-group" >
        <input type = "text"
        className = "form-control form-control-lg"
        placeholder = "Enter Your Salary"
        name = "salary"
        value = { salary }
        onChange = { e => onInputChange(e) }
        /> </div > 
        <div className = "form-group" >
        <input type = "text"
        className = "form-control form-control-lg"
        placeholder = "Enter Your Phone Number"
        name = "phone"
        value = { phone }
        onChange = { e => onInputChange(e) }
        /> </div > 
        <div className = "form-group" >
        <input type = "date" max={dateRestrict}
        className = "form-control form-control-lg"
        placeholder = "Enter Your Date of Birth"
        name = "dob"
        value = { dob }
        onChange = { e => onInputChange(e) }
        /> </div > <button className = "btn btn-warning btn-block" > Update Employee </button> </form > </div> </div >
    );
};

export default EditUser;