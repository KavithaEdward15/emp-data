import React from "react";
import "./App.css";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import Home from "./components/pages/Home.jsx";
import About from "./components/pages/About.jsx";
import Navbar from "./components/layout/Navbar.jsx";
import {
    BrowserRouter as Router,
    Route,
    Switch
} from "react-router-dom";
import AddUser from "./components/users/AddUser.jsx";
import EditUser from "./components/users/EditUser.jsx";

function App(props) {
    return ( <Router>
        <div className = "App" >
        <Navbar/>
        <Switch >
        <Route exact path = "/" component = { Home }/> 
        <Route exact path = "/about" component = { About }/>  
        <Route exact path = "/users/add" component = { AddUser }/> 
        <Route exact path = "/users/edit/:id" component = { EditUser } />
        </Switch > </div> </Router>
    );
}

export default App;